﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
		Me.LabelStatus = New System.Windows.Forms.Label()
		Me.GeckoWebBrowser1 = New Gecko.GeckoWebBrowser()
		Me.SuspendLayout()
		'
		'Timer1
		'
		'
		'LabelStatus
		'
		Me.LabelStatus.AutoSize = True
		Me.LabelStatus.Location = New System.Drawing.Point(13, 13)
		Me.LabelStatus.Name = "LabelStatus"
		Me.LabelStatus.Size = New System.Drawing.Size(30, 25)
		Me.LabelStatus.TabIndex = 1
		Me.LabelStatus.Text = "..."
		'
		'GeckoWebBrowser1
		'
		Me.GeckoWebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.GeckoWebBrowser1.Location = New System.Drawing.Point(0, 0)
		Me.GeckoWebBrowser1.Name = "GeckoWebBrowser1"
		Me.GeckoWebBrowser1.Size = New System.Drawing.Size(1026, 526)
		Me.GeckoWebBrowser1.TabIndex = 2
		Me.GeckoWebBrowser1.UseHttpActivityObserver = False
		'
		'Form1
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1026, 526)
		Me.ControlBox = False
		Me.Controls.Add(Me.LabelStatus)
		Me.Controls.Add(Me.GeckoWebBrowser1)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "Form1"
		Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Form1"
		Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents Timer1 As Timer
	Friend WithEvents LabelStatus As Label
	Friend WithEvents GeckoWebBrowser1 As Gecko.GeckoWebBrowser
End Class
