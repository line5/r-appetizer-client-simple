﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SettingsForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.TextBoxMessendKey = New System.Windows.Forms.TextBox()
		Me.NumericUpDownRefreshInterval = New System.Windows.Forms.NumericUpDown()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Button1 = New System.Windows.Forms.Button()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.TextBoxBaseUrl = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.CheckBoxDebug = New System.Windows.Forms.CheckBox()
		CType(Me.NumericUpDownRefreshInterval, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(29, 76)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(143, 25)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Messend Key"
		'
		'TextBoxMessendKey
		'
		Me.TextBoxMessendKey.Location = New System.Drawing.Point(203, 73)
		Me.TextBoxMessendKey.Name = "TextBoxMessendKey"
		Me.TextBoxMessendKey.Size = New System.Drawing.Size(602, 31)
		Me.TextBoxMessendKey.TabIndex = 1
		'
		'NumericUpDownRefreshInterval
		'
		Me.NumericUpDownRefreshInterval.Location = New System.Drawing.Point(203, 127)
		Me.NumericUpDownRefreshInterval.Maximum = New Decimal(New Integer() {3000, 0, 0, 0})
		Me.NumericUpDownRefreshInterval.Minimum = New Decimal(New Integer() {30, 0, 0, 0})
		Me.NumericUpDownRefreshInterval.Name = "NumericUpDownRefreshInterval"
		Me.NumericUpDownRefreshInterval.Size = New System.Drawing.Size(224, 31)
		Me.NumericUpDownRefreshInterval.TabIndex = 2
		Me.NumericUpDownRefreshInterval.Value = New Decimal(New Integer() {60, 0, 0, 0})
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(29, 129)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(111, 25)
		Me.Label2.TabIndex = 3
		Me.Label2.Text = "Reload (s)"
		'
		'Button1
		'
		Me.Button1.DialogResult = System.Windows.Forms.DialogResult.OK
		Me.Button1.Location = New System.Drawing.Point(676, 230)
		Me.Button1.Name = "Button1"
		Me.Button1.Size = New System.Drawing.Size(129, 49)
		Me.Button1.TabIndex = 4
		Me.Button1.Text = "Speichern"
		Me.Button1.UseVisualStyleBackColor = True
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(90, 188)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(715, 25)
		Me.Label3.TabIndex = 5
		Me.Label3.Text = "Die Einstellungen werden erst nach einem Neustart des Programms aktiv."
		'
		'TextBoxBaseUrl
		'
		Me.TextBoxBaseUrl.Location = New System.Drawing.Point(203, 24)
		Me.TextBoxBaseUrl.Name = "TextBoxBaseUrl"
		Me.TextBoxBaseUrl.Size = New System.Drawing.Size(602, 31)
		Me.TextBoxBaseUrl.TabIndex = 7
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(29, 27)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(109, 25)
		Me.Label4.TabIndex = 6
		Me.Label4.Text = "Base URL"
		'
		'CheckBoxDebug
		'
		Me.CheckBoxDebug.AutoSize = True
		Me.CheckBoxDebug.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.CheckBoxDebug.Location = New System.Drawing.Point(698, 129)
		Me.CheckBoxDebug.Name = "CheckBoxDebug"
		Me.CheckBoxDebug.Size = New System.Drawing.Size(107, 29)
		Me.CheckBoxDebug.TabIndex = 8
		Me.CheckBoxDebug.Text = "Debug"
		Me.CheckBoxDebug.UseVisualStyleBackColor = True
		'
		'SettingsForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(851, 309)
		Me.Controls.Add(Me.CheckBoxDebug)
		Me.Controls.Add(Me.TextBoxBaseUrl)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Button1)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.NumericUpDownRefreshInterval)
		Me.Controls.Add(Me.TextBoxMessendKey)
		Me.Controls.Add(Me.Label1)
		Me.Name = "SettingsForm"
		Me.Text = "Einstellungen"
		Me.TopMost = True
		CType(Me.NumericUpDownRefreshInterval, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents Label1 As Label
	Friend WithEvents TextBoxMessendKey As TextBox
	Friend WithEvents NumericUpDownRefreshInterval As NumericUpDown
	Friend WithEvents Label2 As Label
	Friend WithEvents Button1 As Button
	Friend WithEvents Label3 As Label
	Friend WithEvents TextBoxBaseUrl As TextBox
	Friend WithEvents Label4 As Label
	Friend WithEvents CheckBoxDebug As CheckBox
End Class
