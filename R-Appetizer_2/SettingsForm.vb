﻿Public Class SettingsForm
	Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
		Try
			My.Settings.MessendKey = TextBoxMessendKey.Text
			My.Settings.RefreshInterval = NumericUpDownRefreshInterval.Value
			My.Settings.BaseURL = TextBoxBaseUrl.Text
			My.Settings.Debug = CheckBoxDebug.Checked
			Cursor.Hide()
			My.Settings.Save()
			Me.Close()
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub SettingsForm_Load(sender As Object, e As EventArgs) Handles Me.Load
		TextBoxBaseUrl.Text = My.Settings.BaseURL
		TextBoxMessendKey.Text = My.Settings.MessendKey
		NumericUpDownRefreshInterval.Value = My.Settings.RefreshInterval
		CheckBoxDebug.Checked = My.Settings.Debug
		Cursor.Show()
	End Sub

	Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBoxDebug.CheckedChanged

	End Sub
End Class