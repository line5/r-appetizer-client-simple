﻿Imports System.IO
Imports System.Net
Imports Gecko

Public Class Form1
	Dim baseUrl As String = ""
	Dim currentUrl As String = ""
	Dim messendKey As String = ""
	Dim t1int As Integer = 60

	Public Sub New()
		' This call is required by the designer.
		InitializeComponent()
		' Add any initialization after the InitializeComponent() call.
	End Sub

	Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		t1int = My.Settings.RefreshInterval * 1000
		messendKey = My.Settings.MessendKey
		baseUrl = My.Settings.BaseURL
		loadNewData()
		startTimer()
		Dim CookieMan As nsICookieManager
		CookieMan = Xpcom.GetService(Of nsICookieManager)("@mozilla.org/cookiemanager;1")
		CookieMan = Xpcom.QueryInterface(Of nsICookieManager)(CookieMan)
		CookieMan.RemoveAll()
		If My.Settings.Debug = False Then
			LabelStatus.Hide()
		End If
		GeckoWebBrowser1.Show()
		Cursor.Hide()
	End Sub

	Private Sub startTimer()
		Timer1.Interval = t1int
		Timer1.Start()
	End Sub

	Private Sub debugMsg(msg As String)
		If My.Settings.Debug = True Then
			LabelStatus.Text = msg
			LabelStatus.Refresh()
		End If
	End Sub


	Private Sub loadNewData()
		Try
			If messendKey.Length > 10 Then
				Dim myUrl As String = baseUrl + messendKey + ".txt"
				debugMsg("loading " + myUrl)
				Dim request As HttpWebRequest = WebRequest.Create(myUrl)
				request.Method = "GET"
				request.Timeout = 8000
				request.UserAgent = "R-Appetizer 2.0.0.4"
				Dim destination As String = ""
				Dim webStream As Stream
				Dim Res As HttpWebResponse = CType(request.GetResponse(), HttpWebResponse)
				webStream = Res.GetResponseStream() ' Get Response
				Dim reader As StreamReader = New StreamReader(webStream)
				While reader.Peek >= 0
					destination = reader.ReadToEnd()
				End While

				Dim parts As String() = destination.Split("#")
				If parts.Count > 1 Then
					If parts(1).Length > 8 Then
						Dim newUrl As String = parts(1).Trim

						If newUrl.Substring(0, 4) = "http" Then
							If currentUrl <> newUrl Then
								currentUrl = newUrl
								GeckoWebBrowser1.Navigate(newUrl)
								GeckoWebBrowser1.Document.Body.Style.CssText = "overflow: hidden ! important;"
								GeckoWebBrowser1.Show()
							End If
						End If
					End If
				End If
			End If
		Catch ex As Exception
			'MsgBox(ex.Message)
		End Try
		debugMsg("")
	End Sub

	Private Sub loadNewContent(msg As String)
		Dim parts As String() = msg.Split("#")
		Dim newUrl As String = parts(1).Trim
		If newUrl.Substring(0, 4) = "http" Then
			If currentUrl <> newUrl Then
				GeckoWebBrowser1.Navigate(newUrl)
				GeckoWebBrowser1.Show()
			End If
		End If
	End Sub

	Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
		loadNewData()
	End Sub

	Private Sub Form1_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
		If e.Modifiers = Keys.Control And Chr(e.KeyCode) = "E" Then
			SettingsForm.Show()
		End If
	End Sub

	Private Sub GeckoWebBrowser1_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles GeckoWebBrowser1.PreviewKeyDown
		If e.Modifiers = Keys.Control And Chr(e.KeyCode) = "E" Then
			SettingsForm.Show()
		End If
	End Sub
End Class
