# R-Appetizer Simple Client

Diesen [R-Appetizer](http://www.r-appetizer.com) Client haben wir speziell für 
Kunden erstellt, die auch in 2017 noch digitale Speisekarten mit Windows XP 
betreiben. 

Hinweis: Der Einsatz des Betriebssystemes Windows XP ist aus Sicherheitsgründen 
nicht mehr empfohlen.

## Download

[![Download R-Appetizer Client Simple](https://a.fsdn.com/con/app/sf-download-button)](https://sourceforge.net/projects/r-appetizer-client-simple/files/latest/download)

## Installation

1. ZIP-Datei entpacken
2. .exe-Datei ausführen.
3. Konfigurieren

## Konfiguration

1. Software starten
2. Mit der Tastenkombination Strg+E das Fenster "Einstellungen" öffnen
3. Einstellungen vornehmen
4. **Speichern**-Button klicken
5. Software neu starten!

Einstellungen:

### Base URL und Messend Key

Diese Einstellungen werden Ihnen vom Line5 Support mitgeteilt. 
Für Tüftler: Die Software sucht in der Datei <Baseurl><Messend Key>.txt im angegebenen Intervall nach neuen Instruktionen.

### Intervall

Gibt an, wie oft R-Appetizer nach neuen Inhalten sucht. Der Minimalwert beträgt 30 Sekunden.

## Entwicklung

Die Entwicklung erfolgte durch [Line5](https://www.line5.eu) mittels Visual Studio 2017.

### Prozesse

Bitte nach der Weiterentwicklung zur Veröffentlichung folgende Schritte ausführen:

1. Das Projekt in Visual Studio kompilieren
2. Den Zielordner per ZIP packen
3. Die Zip-Datei nach dem Muster "R-Appetizer-Simple-1.0.zip" benennen.
4. Die Zip-Datei auf Sourceforge veröffentlichen.
5. Eine Release-Tag nach dem Muster "Release/1.0" in Gitlab erstellen. Release Notes einfügen, inklusive Link zum Download des Releases (Sourceforge).

Danke!